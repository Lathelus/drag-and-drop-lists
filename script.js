
var names = ["John", "Mary", "Jack", "Mike", "Philip"];

$(document).ready(function() {

    for (var i = 0; i < names.length; i++) {
        $("#rightList").append("<li>" + names[i] + "</li>");
    }

    $(function () {

        var removeIntent = false;
        var leftListNames = [];
        var isDuplicated = false;

        $("#leftList li")
            .each(function () {
                leftListNames.push($(this).html());
            });

        $("#rightList li").draggable({
            revert: "invalid",
            helper: "clone"
        });


        $("#left")
            .droppable({
                accept: "#rightList li",
                drop: function (event, ui) {
                    var tekst = ui.draggable.text();
                        for (var i = 0; i < leftListNames.length; i++) {
                            if (tekst == leftListNames[i]) {
                                isDuplicated = true;
                                break;
                            }
                            else{
                                isDuplicated = false;
                            }
                        }
                        if(isDuplicated == false) {
                            leftListNames.push(tekst);
                            $("<li></li>")
                                .text(tekst)
                                .appendTo("#leftList")
                    }
                }

            });
        $("#leftList")
            .sortable({
                over: function () {
                    removeIntent = false;
                },
                out: function () {
                    removeIntent = true;
                },
                beforeStop: function (event, ui) {
                    if (removeIntent == true) {
                        ui.item.remove();
                    }
                }
            });
    });
});